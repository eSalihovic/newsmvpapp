//
//  Coordinator.swift
//  NewsMVPApp
//
//  Created by Edin Salihovic on 10/09/2020.
//  Copyright © 2020 None. All rights reserved.
//

import UIKit

protocol Coordinator {
    func start()
    func coordinate(to coordinator: Coordinator)
}

extension Coordinator {
    
    func coordinate(to coordinator: Coordinator) {
        coordinator.start()
    }
}
