//
//  BaseCVDataSource.swift
//  NewsMVPApp
//
//  Created by Edin Salihovic on 13/09/2020.
//  Copyright © 2020 None. All rights reserved.
//

import UIKit

class BaseCVDataSource: NSObject, UICollectionViewDataSource {
    
    private var news = [Article]()
       
    func setNews(_ news: [Article]) {
        self.news = news
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return news.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let newsCell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: NewsCell.self), for: indexPath) as? NewsCell else { return NewsCell() }
        
        newsCell.setupUI(with: news[indexPath.row])
        
        return newsCell
    }
}
