//
//  BaseCVDelegate.swift
//  NewsMVPApp
//
//  Created by Edin Salihovic on 13/09/2020.
//  Copyright © 2020 None. All rights reserved.
//

import UIKit

class BaseCVDelegate: NSObject, UICollectionViewDelegateFlowLayout {
    
    weak var coordinator: NewsFlow?
    var news: [Article]?

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 25, left: 20, bottom: 0, right: 20)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {

        return 40
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let article = news?[indexPath.row] else { return }
        coordinator?.coordinateToDetail(with: article)
    }
}
