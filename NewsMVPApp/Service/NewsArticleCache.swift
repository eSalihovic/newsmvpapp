//
//  NewsArticleCache.swift
//  NewsMVPApp
//
//  Created by Edin Salihovic on 18/09/2020.
//  Copyright © 2020 None. All rights reserved.
//

import Foundation
import Cache

class NewsArticleCache: CacheManager {
    
    init() {
        super.init(interval: 2 * 60 * 60, diskName: .news)
    }
    
    func save(_ newArticle: NewsArticle) {
         let storage = try? Storage(diskConfig: getDiskConfig, memoryConfig: getMemoryConfig, transformer: TransformerFactory.forCodable(ofType: NewsArticle.self)
         )
        try? storage?.setObject(newArticle, forKey: String(describing: NewsArticle.self), expiry: .seconds(getInterval))
    }
    
    func get() -> NewsArticle? {
        let storage = try? Storage(diskConfig: getDiskConfig, memoryConfig: getMemoryConfig, transformer: TransformerFactory.forCodable(ofType: NewsArticle.self)
        )
        
        do {
            let newsArticle = try storage?.object(forKey: String(describing: NewsArticle.self))
            return newsArticle
        }
        catch {
            return nil
        }
    }
}

