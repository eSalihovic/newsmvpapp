//
//  CountryCodeByCarrier.swift
//  NewsMVPApp
//
//  Created by Edin Salihovic on 18/09/2020.
//  Copyright © 2020 None. All rights reserved.
//

import Foundation
import CoreTelephony

// This was a try to detect country code without use of a location manager and asking
// for a permission
// Turns out it can not detect if user is in roaming and it would be overkill because
// if user is visiting France for e.g. but does not know any franch word, it is better
// to use users's phone language

struct CountryCode {
    
    static func getCountryCodeByCarrier() -> String? {
        guard let carriers = CTTelephonyNetworkInfo().serviceSubscriberCellularProviders else { return nil }
        if let first = carriers.first {
            return first.value.isoCountryCode
        }
        else {
            let keys = carriers.keys
            for key in keys {
                guard let code = carriers[key]?.isoCountryCode, !code.isEmpty else { continue }
                return code
             }
        }
        return nil
    }
}

// Soulution below will notify if user changes sim card, not cellular provider due to roaming -> needs more research
/*
 * serviceSubscriberCellularProvidersDidUpdateNotifier
 *
 * Discussion:
 *   A block that will be dispatched on the default priority global dispatch
 *   queue when the subscriber's cellular provider information updates for any service. Set
 *   this property to a block that is defined in your application to receive the newly
 *   updated information.  The NSString will contain the service identifier of the service
 *   whose information has changed.  This can be used as the key into serviceSubscriberCellularProvider
 *   to obtain the new information.
 *
*@available(iOS 12.0, *)
*open var serviceSubscriberCellularProvidersDidUpdateNotifier: ((String) -> Void)?
*/
