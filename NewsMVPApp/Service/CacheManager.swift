//
//  CacheManager.swift
//  NewsMVPApp
//
//  Created by Edin Salihovic on 17/09/2020.
//  Copyright © 2020 None. All rights reserved.
//

import Foundation
import Cache

enum DiskName: String {
    case news = "News"
    case savedNews = "SavedNews"
    case photosOnly = "PhotosOnly"
}

class CacheManager {
    
    private let interval: TimeInterval
    private let diskConfig: DiskConfig
    private let memoryConfig: MemoryConfig
    
    init(interval: TimeInterval, diskName: DiskName) {
        self.interval = interval
        self.diskConfig = DiskConfig(name: diskName.rawValue)
        self.memoryConfig = MemoryConfig(expiry: .seconds(interval), countLimit: 0, totalCostLimit: 0)
    }
    
    var getInterval: TimeInterval {
        return interval
    }
    
    var getDiskConfig: DiskConfig {
        return diskConfig
    }
    
    var getMemoryConfig: MemoryConfig {
        return memoryConfig
    }
}
