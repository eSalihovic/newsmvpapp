//
//  ArticleCache.swift
//  NewsMVPApp
//
//  Created by Edin Salihovic on 18/09/2020.
//  Copyright © 2020 None. All rights reserved.
//

import Foundation
import Cache

class ArticleCache: CacheManager {
    
    init() {
        super.init(interval: 7 * 24 * 60 * 60, diskName: .savedNews)
    }
    
    func save(_ newArticle: Article) {
         let storage = try? Storage(diskConfig: getDiskConfig, memoryConfig: getMemoryConfig, transformer: TransformerFactory.forCodable(ofType: [Article].self)
         )
        
        do {
            let articles = try storage?.object(forKey: String(describing: [Article].self))
            if var articles = articles {
                let containsArticle = articles.contains { (article) -> Bool in
                    return article.url == newArticle.url
                }
                if !containsArticle {
                    articles.insert(newArticle, at: 0)
                }
                try? storage?.setObject(articles, forKey: String(describing: [Article].self), expiry: .seconds(getInterval))
            }
            else {
                try? storage?.setObject([newArticle], forKey: String(describing: [Article].self), expiry: .seconds(getInterval))
            }
            
        }
        catch {
            try? storage?.setObject([newArticle], forKey: String(describing: [Article].self), expiry: .seconds(getInterval))
        }
    }
    
    func get() -> [Article]? {
        let storage = try? Storage(diskConfig: getDiskConfig, memoryConfig: getMemoryConfig, transformer: TransformerFactory.forCodable(ofType: [Article].self)
        )
        
        do {
            let articles = try storage?.object(forKey: String(describing: [Article].self))
            return articles
        }
        catch {
            return nil
        }
    }
}
