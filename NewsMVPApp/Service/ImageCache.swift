//
//  ImageCache.swift
//  NewsMVPApp
//
//  Created by Edin Salihovic on 18/09/2020.
//  Copyright © 2020 None. All rights reserved.
//

import Foundation
import Cache

class ImageCache: CacheManager {
    
    init() {
        super.init(interval: 7 * 24 * 60 * 60, diskName: .photosOnly)
    }
    
    func save(_ image: UIImage, forKey key: String) {
        let storage = try? Storage(diskConfig: getDiskConfig, memoryConfig: getMemoryConfig, transformer: TransformerFactory.forImage())
        try? storage?.setObject(image, forKey: key, expiry: .seconds(getInterval))
    }
    
    func getImage(forKey key: String) -> UIImage? {
        let storage = try? Storage(diskConfig: getDiskConfig, memoryConfig: getMemoryConfig, transformer: TransformerFactory.forImage())

        do {
            let image = try storage?.object(forKey: key)
            return image
        }
        catch {
            return nil
        }
    }
}
