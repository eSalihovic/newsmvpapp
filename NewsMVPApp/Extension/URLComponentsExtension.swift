//
//  URLComponentsExtension.swift
//  NewsMVPApp
//
//  Created by Edin Salihovic on 15/09/2020.
//  Copyright © 2020 None. All rights reserved.
//

import Foundation

extension URLComponents {
    
    init(scheme: String = Endpoint.scheme,
         host: String = Endpoint.baseUrl,
         path: String) {
        self.init()
        self.scheme = scheme
        self.host = host
        self.path = path
    }
    
    mutating func setQueryItems(with parameters: [String: String]) {
        self.queryItems = parameters.map { URLQueryItem(name: $0.key, value: $0.value) }
    }
}
