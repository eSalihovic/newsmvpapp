//
//  UICollectionViewExtension.swift
//  NewsMVPApp
//
//  Created by Edin Salihovic on 16/09/2020.
//  Copyright © 2020 None. All rights reserved.
//

import UIKit

enum CollectionViewElementKind {
    case header
    case footer
}

extension UICollectionView {
    
    func registerCell(with identifier: String) {
        self.register(UINib(nibName: identifier, bundle: nil), forCellWithReuseIdentifier: identifier)
    }
    
    func registerSupplementaryView(with identifier: String, and kind: CollectionViewElementKind) {
        let elementKind = kind == .header ? UICollectionView.elementKindSectionHeader : UICollectionView.elementKindSectionFooter
        self.register(UINib(nibName: identifier, bundle: nil), forSupplementaryViewOfKind: elementKind, withReuseIdentifier: identifier)
    }
}

extension UICollectionReusableView {
    
    static var identifier: String {
        return String(describing: self)
    }
}
