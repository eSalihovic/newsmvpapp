//
//  UIImageViewExtension.swift
//  NewsMVPApp
//
//  Created by Edin Salihovic on 17/09/2020.
//  Copyright © 2020 None. All rights reserved.
//

import UIKit

extension UIImageView {
    
    func loadImage(from urlString: String) {
        
        let placeholder = UIImage(named: "noImage")
        let imageCache = ImageCache()
        
        if let img = imageCache.getImage(forKey: urlString) {
            image = img
        }
        else {
            guard let url = URL(string: urlString) else {
                image = placeholder
                return
            }
            
            DispatchQueue.global().async { [weak self] in
                do {
                    let data = try Data(contentsOf: url)
                    DispatchQueue.main.async {
                        guard let img = UIImage(data: data) else {
                            self?.image = placeholder
                            return
                        }
                        imageCache.save(img, forKey: urlString)
                        self?.image = img
                        
                    }
                }
                catch {
                    DispatchQueue.main.async {
                        self?.image = placeholder
                    }
                }
                
            }
        }
    }
}
