//
//  TabBarCoordinator.swift
//  NewsMVPApp
//
//  Created by Edin Salihovic on 10/09/2020.
//  Copyright © 2020 None. All rights reserved.
//

import UIKit

class TabBarCoordinator: Coordinator {
    
    let window: UIWindow
    
    init(window: UIWindow) {
        self.window = window
    }
    
    func start() {
        let tabBarController = TabBarController()
        window.rootViewController = tabBarController
        window.makeKeyAndVisible()
        
        let newsNavigationController = UINavigationController()
        newsNavigationController.navigationBar.barTintColor = .white
        
        newsNavigationController.tabBarItem = UITabBarItem(tabBarSystemItem: .topRated, tag: 0)
        let newsCoordinator = NewsCoordinator(navigationController: newsNavigationController)
        
        let historyNavigationController = UINavigationController()
        historyNavigationController.navigationBar.isHidden = true
        historyNavigationController.tabBarItem = UITabBarItem(tabBarSystemItem: .history, tag: 1)
        let historyCoordinator = HistoryCoordinator(navigationController: historyNavigationController)
        
        tabBarController.viewControllers = [newsNavigationController, historyNavigationController]
        
        coordinate(to: newsCoordinator)
        coordinate(to: historyCoordinator)
    }
}
