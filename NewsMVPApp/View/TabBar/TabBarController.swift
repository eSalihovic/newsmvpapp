//
//  TabBarVC.swift
//  NewsMVPApp
//
//  Created by Edin Salihovic on 10/09/2020.
//  Copyright © 2020 None. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tabBar.layer.masksToBounds = true
        tabBar.barStyle = .black
        tabBar.barTintColor = .white
        tabBar.tintColor = UIColor.orange
        
        tabBar.layer.shadowColor = UIColor.lightGray.cgColor
        tabBar.layer.shadowOffset = CGSize(width: 0.0, height: 4.0)
        tabBar.layer.shadowRadius = 10
        tabBar.layer.shadowOpacity = 1
        tabBar.layer.masksToBounds = false
    }
}
