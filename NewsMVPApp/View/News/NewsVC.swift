//
//  NewsVC.swift
//  NewsMVPApp
//
//  Created by Edin Salihovic on 10/09/2020.
//  Copyright © 2020 None. All rights reserved.
//

import UIKit

class NewsVC: UIViewController, Storyboarded {
    
    @IBOutlet private weak var collectionView: UICollectionView!
    
    var coordinator: NewsFlow?
    
    private let viewModel = NewsViewModel()
    private let searchController = UISearchController(searchResultsController: nil)
    
    private var refreshControl = UIRefreshControl()
    private var searchBarIsEmpty: Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    private var isFiltering: Bool {
        return searchController.isActive && !searchBarIsEmpty
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.registerSupplementaryView(with: HotNewsHeader.identifier, and: .header)
        collectionView.registerCell(with: NewsCell.identifier)
        collectionView.delegate = viewModel.getDelegate
        collectionView.dataSource = viewModel.getDatasource
        
        refreshControl.addTarget(self, action: #selector(self.refresh), for: .valueChanged)
        collectionView.addSubview(refreshControl)
        
        viewModel.coordinator = coordinator
        viewModel.handleSuccess = { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.updateUI()
        }
        viewModel.handleError = { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.handleError()
        }
        
        Hud.startHud(style: .dark, text: Strings.emptyString, inView: view)
        viewModel.loadAllNews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupNavBar()
    }
}

private extension NewsVC {
    
    func setupNavBar() {
        searchController.searchResultsUpdater = self
        searchController.searchBar.delegate = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search news by specific word."

        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.orange]
        navigationItem.searchController = searchController
        definesPresentationContext = true
    }
    
    func updateUI() {
        Hud.displaySuccess()
        refreshControl.endRefreshing()
        viewModel.updateDelegateAndDataSource(withHeaderVisibility: !isFiltering)
        collectionView.reloadData()
    }
    
    func handleError() {
        Hud.displayError()
        refreshControl.endRefreshing()
        updateUI()
    }
    
    @objc func refresh() {
        Hud.startHud(style: .dark, text: Strings.emptyString, inView: view)
        viewModel.loadAllNews()
    }
}

extension NewsVC: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        guard let text = searchController.searchBar.text, text.count >= 3 else { return }
        viewModel.searchNews(searchFrase: text)
    }
}

extension NewsVC: UISearchBarDelegate {
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        viewModel.updateDelegateAndDataSource()
        collectionView.reloadData()
    }
}
