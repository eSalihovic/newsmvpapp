//
//  HotNewsDelegate.swift
//  NewsMVPApp
//
//  Created by Edin Salihovic on 13/09/2020.
//  Copyright © 2020 None. All rights reserved.
//

import UIKit

protocol PageControlDelegate: class {
    func setCurrentDot(with pageNumber: Int)
}

class HotNewsDelegate: BaseCVDelegate {
    
    private let cacheManager = ArticleCache()
    
    weak var pageControlDelegate: PageControlDelegate?
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width - 40, height: collectionView.frame.size.height - 50)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = scrollView.contentOffset.x / scrollView.frame.size.width
        pageControlDelegate?.setCurrentDot(with: Int(pageNumber))
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let article = news?[indexPath.row] else { return }
        cacheManager.save(article)
        coordinator?.coordinateToDetail(with: article)
    }
}
