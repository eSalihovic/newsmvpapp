//
//  NewsDataSource.swift
//  NewsMVPApp
//
//  Created by Edin Salihovic on 11/09/2020.
//  Copyright © 2020 None. All rights reserved.
//

import UIKit

class NewsDataSource: BaseCVDataSource {
    
    private var hotNews = [Article]()
    
    weak var coordinator: NewsFlow?
    
    func setHotNews(_ hotNews: [Article]) {
        self.hotNews = hotNews
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {

        guard let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: String(describing: HotNewsHeader.self), for: indexPath) as? HotNewsHeader else { return UICollectionReusableView() }

        header.setDataSourceAndCoordinator(hotNews, and: coordinator)
        
        return header
    }
}
