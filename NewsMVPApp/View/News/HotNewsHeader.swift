//
//  HotNewsHeader.swift
//  NewsMVPApp
//
//  Created by Edin Salihovic on 11/09/2020.
//  Copyright © 2020 None. All rights reserved.
//

import UIKit

class HotNewsHeader: UICollectionReusableView {
    
    @IBOutlet private weak var pageControl: UIPageControl!
    @IBOutlet private weak var collectionView: UICollectionView! {
        didSet {
            collectionView.register(UINib(nibName: String(describing: NewsCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: NewsCell.self))
            collectionView.delegate = delegate
            collectionView.dataSource = dataSource
        }
    }
    
    private var delegate: HotNewsDelegate? = HotNewsDelegate()
    private var dataSource: BaseCVDataSource?  = BaseCVDataSource()
    
    deinit {
        delegate = nil
        dataSource = nil
    }
    
    func setDataSourceAndCoordinator(_ hotNews: [Article], and coordinator: NewsFlow?) {
        delegate?.pageControlDelegate = self
        delegate?.news = hotNews
        delegate?.coordinator = coordinator
        dataSource?.setNews(hotNews)
        collectionView?.reloadData()
    }
}

extension HotNewsHeader: PageControlDelegate {
    func setCurrentDot(with pageNumber: Int) {
        pageControl.currentPage = pageNumber
    }
}
