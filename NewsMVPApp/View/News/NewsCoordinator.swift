//
//  NewsCoordinator.swift
//  NewsMVPApp
//
//  Created by Edin Salihovic on 10/09/2020.
//  Copyright © 2020 None. All rights reserved.
//

import UIKit

protocol NewsFlow: class {
    func coordinateToDetail(with article: Article)
}

class NewsCoordinator: Coordinator, NewsFlow {
    
    weak var navigationController: UINavigationController?
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let newsVC = NewsVC.instantiate()
        newsVC.coordinator = self
        navigationController?.pushViewController(newsVC, animated: false)
    }
    
    // MARK: - Flow methods
    
    func coordinateToDetail(with article: Article) {
        let newsDetailCoordinator = NewsDetailCoordinator(navigationController: navigationController!, article: article)
        coordinate(to: newsDetailCoordinator)
    }
}
