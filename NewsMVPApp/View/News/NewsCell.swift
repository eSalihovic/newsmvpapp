//
//  NewsCell.swift
//  NewsMVPApp
//
//  Created by Edin Salihovic on 11/09/2020.
//  Copyright © 2020 None. All rights reserved.
//

import UIKit

class NewsCell: UICollectionViewCell {
    
    @IBOutlet private var articleImageView: UIImageView!
    @IBOutlet private var articleTitleLabel: UILabel!
    @IBOutlet private var articleDescriptionLabel: UILabel!
    @IBOutlet private var authorNameLabel: UILabel!
    
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        articleImageView.image = nil
        articleTitleLabel.text = nil
        articleDescriptionLabel.text = nil
        authorNameLabel.text = nil
    }
    
    func setupUI(with article: Article) {
        contentView.translatesAutoresizingMaskIntoConstraints = false
        contentView.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.size.width - 40).isActive = true
        articleTitleLabel.text = article.title
        articleDescriptionLabel.text = article.description
        authorNameLabel.text = article.author
        guard let urlToImage = article.urlToImage else { return }
        articleImageView.loadImage(from: urlToImage)
    }
}
