//
//  NewsDelegate.swift
//  NewsMVPApp
//
//  Created by Edin Salihovic on 13/09/2020.
//  Copyright © 2020 None. All rights reserved.
//

import UIKit

class NewsDelegate: BaseCVDelegate {
    
    private let cacheManager = ArticleCache()
    
    var isHeaderVisible = true
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {

        return CGSize(width: collectionView.bounds.size.width, height: isHeaderVisible ? 450 : 0)
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let article = news?[indexPath.row] else { return }
        cacheManager.save(article)
        coordinator?.coordinateToDetail(with: article)
    }
}
