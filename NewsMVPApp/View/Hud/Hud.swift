//
//  Hud.swift
//  NewsMVPApp
//
//  Created by Edin Salihovic on 16/09/2020.
//  Copyright © 2020 None. All rights reserved.
//

import UIKit
import JGProgressHUD

open class Hud {
    
    static var hud: JGProgressHUD?
    
    public static func startHud(style: JGProgressHUDStyle, text: String, inView: UIView) {
        hud = JGProgressHUD(style: style)
        hud?.textLabel.text = text
        hud?.show(in: inView)
    }
    
    public static func displaySuccess() {
        guard let hud = hud else { return }
        showHUDWithTransform(hud: hud, title: "Done", indicatorView: JGProgressHUDSuccessIndicatorView())
    }
    
    public static func stopHud() {
        hud?.dismiss()
    }
    
    public static func displayError() {
        guard let hud = hud else { return }
        showHUDWithTransform(hud: hud, title: "Failed", indicatorView: JGProgressHUDErrorIndicatorView())
    }
    
    private static func showHUDWithTransform(hud: JGProgressHUD, title: String, indicatorView: JGProgressHUDIndicatorView) {
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(1000)) {
            UIView.animate(withDuration: 0.2) {
                hud.indicatorView = indicatorView
                hud.textLabel.font = UIFont.systemFont(ofSize: 30.0)
                hud.textLabel.text = title
                hud.position = .bottomCenter
            }
        }
        
        hud.dismiss(afterDelay: 1.0)
    }
}
