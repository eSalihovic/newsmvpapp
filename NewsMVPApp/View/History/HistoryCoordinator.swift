//
//  HistoryCoordinator.swift
//  NewsMVPApp
//
//  Created by Edin Salihovic on 10/09/2020.
//  Copyright © 2020 None. All rights reserved.
//

import UIKit

class HistoryCoordinator: Coordinator, NewsFlow {
    
    weak var navigationController: UINavigationController?
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let historyVC = HistoryVC.instantiate()
        historyVC.coordinator = self
        
        navigationController?.pushViewController(historyVC, animated: false)
    }
    
    // MARK: - Flow Methods
    
    func coordinateToDetail(with article: Article) {
        let newsDetailCoordinator = NewsDetailCoordinator(
            navigationController: navigationController!,
            article: article
        )
        
        coordinate(to: newsDetailCoordinator)
    }
}
