//
//  HistoryVC.swift
//  NewsMVPApp
//
//  Created by Edin Salihovic on 10/09/2020.
//  Copyright © 2020 None. All rights reserved.
//

import UIKit

class HistoryVC: UIViewController, Storyboarded {
    
    @IBOutlet private weak var collectionView: UICollectionView!
    
    var coordinator: NewsFlow?
    
    private var viewModel = HistoryViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.register(UINib(nibName: String(describing: NewsCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: NewsCell.self))
        collectionView.delegate = viewModel.getDelegate
        collectionView.dataSource = viewModel.getDataSource
        viewModel.coordinator = coordinator
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        viewModel.updateDelegateAndDataSource()
        collectionView.reloadData()
    }
}
