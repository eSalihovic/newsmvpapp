//
//  NewsDetailVC.swift
//  NewsMVPApp
//
//  Created by Edin Salihovic on 10/09/2020.
//  Copyright © 2020 None. All rights reserved.
//

import UIKit

class NewsDetailVC: UIViewController, Storyboarded {
    
    @IBOutlet private weak var articleImageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet private weak var contentLabel: UILabel!
    @IBOutlet private weak var publicationDateLabel: UILabel!
    @IBOutlet private weak var authorNameLabel: UILabel!
    
    var coordinator: NewsDetailCoordinator?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.text = coordinator?.article.title
        descriptionLabel.text = coordinator?.article.description
        contentLabel.text = coordinator?.article.content
        publicationDateLabel.text = coordinator?.article.publishedAt
        authorNameLabel.text = coordinator?.article.author
        guard let url = coordinator?.article.urlToImage else {
            articleImageView.image = UIImage(named: "noImage")
            return
        }
        articleImageView.loadImage(from: url)
    }
    
    @IBAction func closeButtonTapped(_ sender: UIButton) {
        coordinator?.dismissDetail()
    }
}
