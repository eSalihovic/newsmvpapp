//
//  NewsDetailCoordinator.swift
//  NewsMVPApp
//
//  Created by Edin Salihovic on 10/09/2020.
//  Copyright © 2020 None. All rights reserved.
//

import UIKit

//protocol NewsDetailFlow: class {
//    func dismissDetail()
//}

class NewsDetailCoordinator: Coordinator {
    
    let navigationController: UINavigationController
    let article: Article
    
    init(navigationController: UINavigationController,
         article: Article) {
        
        self.navigationController = navigationController
        self.article = article
    }
    
    func start() {
        let newsDetailVC = NewsDetailVC.instantiate()
        newsDetailVC.coordinator = self
        
        navigationController.present(newsDetailVC, animated: true, completion: nil)
    }
    
    // MARK: - Flow Methods
    
    func dismissDetail() {
        navigationController.dismiss(animated: true, completion: nil)
    }
}
