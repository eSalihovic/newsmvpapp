//
//  Constants.swift
//  NewsMVPApp
//
//  Created by Edin Salihovic on 11/09/2020.
//  Copyright © 2020 None. All rights reserved.
//

import Foundation

// should be stored on a remote server and received in payload of a silent notification
let APIKEY = "0bb5451d20bf436f9f6af80fdebbe784"

struct Endpoint {
    static let scheme = "https"
    static let baseUrl = "https://newsapi.org/v2/"
    static let topHeadlines = baseUrl + "top-headlines"
    static let everithing = baseUrl +  "everything"
}

struct CountryCodes {
    static let defaultCode = "us"
    static let availableCodes = ["ae", "ar", "at", "au", "be", "bg", "br", "ca", "ch", "cn", "co", "cu", "cz", "de", "eg", "fr", "gb", "gr", "hk", "hu", "id", "ie", "il", "in", "it", "jp", "kr", "lt", "lv", "ma", "mx", "my", "ng", "nl", "no", "nz", "ph", "pl", "pt", "ro", "rs", "ru", "sa", "se", "sg", "si", "sk", "th", "tr", "tw", "ua", "us", "ve", "za"]
}

struct Strings {
    static let emptyString = ""
}

struct Defaults {
    static let userLocation = "UserLocation"
}
