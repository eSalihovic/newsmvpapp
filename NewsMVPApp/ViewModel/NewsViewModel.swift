//
//  NewsViewModel.swift
//  NewsMVPApp
//
//  Created by Edin Salihovic on 11/09/2020.
//  Copyright © 2020 None. All rights reserved.
//

import Foundation
import Alamofire

class NewsViewModel {
    
    private let kNumberOfHotNews = 5
    private let cacheManager = NewsArticleCache()
    
    private var delegate = NewsDelegate()
    private var dataSource = NewsDataSource()
    private var newsArticle: NewsArticle? {
        set {
            guard let article = newValue else { return }
            cacheManager.save(article)
        }
        get {
            return cacheManager.get()
        }
    }
    
    var handleSuccess: () -> Void = {}
    var handleError: () -> Void = {}
    
    weak var coordinator: NewsFlow?
    
    var getDelegate: NewsDelegate {
        return delegate
    }
    
    var getDatasource: NewsDataSource {
        return dataSource
    }
    
    var getHotNews: [Article] {
        guard let articles = newsArticle?.articles else { return [Article]() }
        return Array(articles.prefix(kNumberOfHotNews))
    }
    
    var getNews: [Article] {
        guard let articles = newsArticle?.articles else { return [Article]() }
        guard articles.count > kNumberOfHotNews else { return articles }
        return Array((articles[kNumberOfHotNews..<articles.count]))
    }
    
    func updateDelegateAndDataSource(withHeaderVisibility isVisible: Bool = true) {
        delegate.isHeaderVisible = isVisible
        dataSource.setNews(getNews)
        dataSource.setHotNews(getHotNews)
        dataSource.coordinator = coordinator
        delegate.news = getNews
        delegate.coordinator = coordinator
    }
    
    func loadAllNews() {
        
        let queryParams: [String: String] = [
            "country": getCountryCode(),
            "apiKey": APIKEY
        ]
        loadNews(from: Endpoint.topHeadlines, with: queryParams)
    }
    
    func searchNews(searchFrase: String) {
        
        let queryParams: [String: String] = [
            "q": searchFrase,
            "apiKey": APIKEY
        ]
        loadNews(from: Endpoint.everithing, with: queryParams)
    }
    
    private func loadNews(from: String, with params: [String: String]) {
        AF.request(from, method: .get, parameters: params).validate(statusCode: 200..<300).responseDecodable { [weak self] (response: DataResponse<NewsArticle, AFError>) in
            guard let strongSelf = self else { return }
            switch response.result {
            case .success(let news):
                strongSelf.newsArticle = news
                strongSelf.handleSuccess()
            case .failure( _):
                strongSelf.newsArticle = strongSelf.cacheManager.get()
                strongSelf.handleError()
            }
        }
    }
    
    private func getCountryCode() -> String {
        guard let countryCode = NSLocale.current.regionCode else { return CountryCodes.defaultCode }
        return CountryCodes.availableCodes.contains(countryCode.lowercased()) ? countryCode.lowercased() : CountryCodes.defaultCode
    }
}
