//
//  HistoryViewModel.swift
//  NewsMVPApp
//
//  Created by Edin Salihovic on 11/09/2020.
//  Copyright © 2020 None. All rights reserved.
//

import Foundation

class HistoryViewModel {
    
    private let cacheManager = ArticleCache()
    
    private var delegate = BaseCVDelegate()
    private var dataSource = BaseCVDataSource()
    private var getArticles: [Article]? {
        return cacheManager.get()
    }
    
    var getDelegate: BaseCVDelegate {
        return delegate
    }
    
    var getDataSource: BaseCVDataSource {
        return dataSource
    }
    
    weak var coordinator: NewsFlow?
    
    func updateDelegateAndDataSource() {
        dataSource.setNews(getArticles ?? [Article]())
        delegate.news = getArticles
        delegate.coordinator = coordinator
    }
}
